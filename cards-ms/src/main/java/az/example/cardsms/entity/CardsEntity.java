package az.example.cardsms.entity;

import lombok.Data;

@Data
public class CardsEntity {
    private String id;
    private String guid;
    private String panMask;
}
